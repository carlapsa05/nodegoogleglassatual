var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { caminhoImg: "'/images/glass-oculos-preto-peq.png'", desenvolvedor: "Carla Patrícia 2 " });
});
router.get('/faleconosco', function(req, res, next) {
  res.render('faleconosco', { caminhoImg: "'/images/glass-oculos-preto-peq.png'"});
});
router.get('/multimidia', function(req, res, next) {
  res.render('multimidia', { caminhoImg: "'/images/multimidia.png'"});
});
router.get('/fotos', function(req, res, next) {
  res.render('fotos', { caminhoImg: "'/images/fotos.png'"});
});
router.get('/specs', function(req, res, next) {
  res.render('specs', { caminhoImg: "'/images/especificacoes.png'"});
});

module.exports = router;